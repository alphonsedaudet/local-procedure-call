# Sujet 

** https://moodle.u-paris.fr/pluginfile.php/854931/mod_resource/content/3/projet2021.pdf


# Local Procedure Call

Remote Procedure Call (rpc) est une interface qui permet d’appeler une fonction sur une
machine distante.
Inspiré par rpc, le projet demande d’implémenter une interface Local Procedure Call
(lpc) qui, intuitivement, permet à un processus client d’appeler une fonction implémentée
dans un autre processus, le processus serveur.
Évidemment cela pose un problème de transmission : comment passer les paramètres, et
comment récupérer les résultats d’un tel appel.
Les processus utiliseront pour cela la mémoire partagée obtenue par l’image mémoire d’un
shared memory object (ShMO), qui sera créé par le serveur.


# Le client

* (a) lpc_call « réveille » le serveur (sans doute à l’aide de pthread_cond_signal) et se
suspend par un appel à pthread_cond_wait ;

* (b) le serveur appelle la fonction appropriée ;

* (c) quand cette fonction termine, le serveur recopie les nouvelles valeurs des paramètres
(dans notre exemple, les nouvelles valeurs int, double et lpc_string) dans la mémoire
partagée et il « réveille » le client par un appel à pthread_cond_signal ;

* (d) le client recopie les nouvelles valeurs depuis la mémoire partagée vers les adresses
données en paramètres de lpc_call ;

* (e) l’appel à lpc_call termine.


#  Gestion d’erreurs

Comme le but de lpc_call est de simuler le plus fidèlement possible un appel de fonction
normal, il faut aussi simuler le comportement d’un appel de fonction qui provoque une
erreur.
Supposons par exemple que le serveur exécute pour un client une fonction fun qui utilise
l’appel système open, et que cet appel échoue. La fonction fun retournera −1 et dans ce
cas le serveur doit copier la valeur de errno dans la mémoire partagée, et ensuite lpc_call
doit copier cette valeur depuis la mémoire partagée dans la variable errno du processus
client.


# Serveur distribué

Le projet tel qu’il est présenté ci-dessus propose une implémentation simple de lpc. Cette
implémentation a un défaut majeur : à tout instant, au plus un appel de fonction client
est effectivement exécuté par le serveur, et donc un seul processus client est servi par le
serveur. Si l’exécution de la fonction client est longue, les autres clients éventuels restent
bloqués sur lpc_call.
Le serveur simple présenté dans les sections précédentes permet de valider le projet, mais
pour obtenir une très bonne note il faut développer un serveur distribué capable de répondre à plusieurs appels de fonction simultanés.
La suite de cette section décrit le comportement du serveur distribué.
Quand le serveur reçoit un appel lpc, il crée un processus enfant, qui se chargera de
l’exécution de la fonction client et de toute la communication avec le client. Pendant ce
temps, le serveur peut répondre à d’autres appels de fonction de la part d’autres clients.
Il reste un problème à résoudre : les différents clients ne peuvent pas mettre les données
dans la même mémoire ou plus exactement dans le même emplacement dans la mémoire.
Une solution consisterait à subdiviser la partie DATA de la mémoire partagée en segments,
chaque segment correspondant à un appel de fonction. C’est difficile à gérer. On propose
ici une autre solution.
Soit shmo_name le nom du shared memory object utilisé par l’implémentation simple
de lpc.
Quand le client exécute lpc_call avec un serveur distribué, le client utilise la mémoire
partagée liée à shmo_name pour passer son pid au serveur.
L’enfant du serveur (celui qui doit gérer l’appel du client) crée alors un nouveau shared
memory object dont le nom est obtenu en concatenant shmo_name avec le pid du client.
Nous appelons ce nouveau shared memory object ShMO de communication.
Le processus client crée aussi l’image mémoire du ShMO de communication. Tout passage
de données entre le client et l’enfant du serveur se fera en utilisant cette mémoire partagée.
Le format de cette mémoire partagée est exactement le même que pour le serveur simple.
En fait à partir de ce moment la communication entre le processus client et l’enfant du
serveur est la même que la communication entre le serveur simple et le client, c’est-à-dire que le client et le processus enfant du serveur interagissent comme dans le cas d’un serveur simple.

Une fois que lpc_call a transmis les résultats de lpc vers la mémoire du client et avant
de terminer, lpc_call doit indiquer au processus enfant du serveur que le transfert est
fini (un flag dans la mémoire partagée). L’enfant du serveur détruit alors l’objet shmo de
communication et termine.

Le serveur devra de temps en temps supprimer les enfants zombies.
