#include "lpc_type.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

lpc_string *lpc_make_string(const char *s, int taille)
{
	lpc_string *str_ptr =NULL;
	if (taille > 0 && s == NULL)
	{
		if ((str_ptr = malloc(sizeof(lpc_string) + taille + 1)) == NULL)
			perror("malloc");
		str_ptr->slen = taille;
		strcpy(str_ptr->string, memset((void *)s, 0, taille));
		return str_ptr;
	}
	else if (taille <= 0 && s != NULL)
	{
		if ((str_ptr = malloc(sizeof(lpc_string) + strlen(s)  + 1)) == NULL)
			perror("malloc");
		str_ptr->slen = strlen(s) + 1;
		strcpy(str_ptr->string, s);
		return str_ptr;
	}
	else if (taille > (strlen(s) + 1))
	{
		if ((str_ptr = malloc(sizeof(lpc_string) + taille + 1)) == NULL)
			perror("malloc");
		str_ptr->slen = taille;
		strcpy(str_ptr->string, s);
		return str_ptr;
	}
	else
	{
		return NULL;
	}
}