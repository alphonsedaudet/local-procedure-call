#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

char *prefix_slash(const char *name){
  #define L_NAME 256
  char *nom=malloc(L_NAME);
  if( name[1] != '/' ){
    nom[0] = '/';
    nom[1]='\0';
    strncat(nom, name, L_NAME-3);
  }else{
    strncpy(nom, name, L_NAME);
    nom[L_NAME-1]='\0';
  }
  return nom;
}


int initialiser_mutex(pthread_mutex_t *pmutex){
    pthread_mutexattr_t mutexattr;
  int code;

  code = pthread_mutexattr_init(&mutexattr) ;
  if( code != 0 )
    return code;

  code = pthread_mutexattr_setpshared(&mutexattr,PTHREAD_PROCESS_SHARED) ;	    
  if( code != 0 )
    return code;
  
  code = pthread_mutex_init(pmutex, &mutexattr);
  return code;
}

int initialiser_cond(pthread_cond_t *pcond){
  pthread_condattr_t condattr;
  int code;
  code = pthread_condattr_init(&condattr);
  if( code != 0 )
    return code;
  
  code = pthread_condattr_setpshared(&condattr,PTHREAD_PROCESS_SHARED);
  if( code != 0 )
    return code;
  
  code = pthread_cond_init(pcond, &condattr) ; 
  return code;
}	

void thread_error_exit(const char *file, int line, int code, char *txt){
  if( txt != NULL )
    fprintf( stderr,  "[%s] in file %s in line %d :  %s\n",
	     txt, file , line, strerror( code ) );
  else
    fprintf( stderr,  "in file %s in line %d :  %s\n",
	     file , line, strerror( code ) );
  exit(1);
}

void thread_error(const char *file, int line, int code, char *txt){
  if( txt != NULL )
    fprintf( stderr,  "[pid %d] [%s] in file %s in line %d :  %s\n",(int) getpid(), txt, file , line, strerror( code ) );
  else
    fprintf( stderr,  "[pid %d] in file %s in line %d :  %s\n", (int) getpid(), file , line, strerror( code ) );
  exit(1);
}