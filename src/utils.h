#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define PANIC_EXIT( msg )  do{			\
   fprintf(stderr,\
     "\n %d %s : error \"%s\" in file %s in line %d\n",\
	   (int) getpid(), msg, strerror(errno), __FILE__, __LINE__);	\
   exit(1);\
  } while(0)	

#define PANIC( msg )  do{			\
   fprintf(stderr,\
     "\n %d %s : error \"%s\" in file %s in line %d\n",\
	   (int) getpid(),msg, strerror(errno), __FILE__, __LINE__);	 \
  } while(0)

char *prefix_slash(const char *name);
int initialiser_mutex(pthread_mutex_t *pmutex);
int initialiser_cond(pthread_cond_t *pcond);
extern void thread_error_exit(const char *file, int line, int code, char *txt);
void thread_error(const char *file, int line, int code, char *txt);

#endif /* UTILS_H_ */
