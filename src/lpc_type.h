/*
 * lpc_type.h
 *
 *  Created on: April 14, 2021
 *      Author: daudet
 */

#ifndef LPC_TYPE_H_
#define LPC_TYPE_H_

#include <stddef.h>
#include <pthread.h>
#include <stdbool.h>
#define NAMELEN 48
#define MAXPARAM 50

typedef enum
{
	STRING,
	DOUBLE,
	INT,
	NOP
} lpc_type;

typedef struct
{
	lpc_type type;
	void *param;
} lpc_param;

typedef struct
{
	int slen;
	char string[];
} lpc_string;

typedef enum
{
	CLIENT,
	SERVER
} lpc_turn;

typedef struct
{
	bool request;
	pthread_mutex_t mutex;
	pthread_cond_t client_cond;
	pthread_cond_t server_cond;
	char momory_name[NAMELEN];
	pid_t client_pid;
} synch_memory;

typedef struct
{
	/* synchonisation data */
	lpc_turn turn;
	pthread_mutex_t mutex;
	pthread_cond_t client_cond;
	pthread_cond_t server_cond;

	/*data processing */
	int response;
	int errno_code;
	char memory_name[NAMELEN];
	char fun_name[NAMELEN];
	char data[1000];
} memory;

/**
 * permet la creation d'un objet STRING
 */
lpc_string *lpc_make_string(const char *s, int taille);

#endif /* LPC_TYPE_H_ */
