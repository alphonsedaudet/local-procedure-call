/*
 * lpc_server.h
 *
 *  Created on: Mar 29, 2021
 *      Author: daudet
 */

#ifndef LPC_SERVER_H_
#define LPC_SERVER_H_

#include <stddef.h>
#define NAMELEN 48

typedef struct
{
	char fun_name[NAMELEN];
	int (*fun)(void *);
} lpc_function;

void *lpc_create(const char *nom, size_t capacite);
void *lpc_get_function(const char *nom);

#endif /* LPC_SERVER_H_ */
