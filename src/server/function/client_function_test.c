

/*
 * client_function_test.c
 * Quelques fonctions client pour tester le fonctionnement de LPC
 */

#include <stddef.h>
#include "../../lpc_type.h"
#include "client_function_test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>

int permute_int(void *param)
{
    int a, b, tmp;
    int next = 0;
    memcpy(&a, ((uint8_t *)param + next), sizeof(int));
    next += sizeof(int);
    memcpy(&b, ((uint8_t *)param + next), sizeof(int));
    next = 0;
    tmp = a;
    a = b;
    b = tmp;
    memcpy(((uint8_t *)param + next), &a, sizeof(int));
    next += sizeof(int);
    memcpy(((uint8_t *)param + next), &b, sizeof(int));
    return 0;
}

int permute_double(void *param)
{
    double a, b, tmp;
    int next = 0;
    memcpy(&a, ((uint8_t *)param + next), sizeof(double));
    next += sizeof(double);
    memcpy(&b, ((uint8_t *)param + next), sizeof(double));
    tmp = a;
    a = b;
    b = tmp;
    next = 0;
    memcpy(((uint8_t *)param + next), &a, sizeof(double));
    next += sizeof(double);
    memcpy(((uint8_t *)param + next), &b, sizeof(double));
    return 0;
}

int concat_string(void *param)
{

    size_t next = 0;
    int slen1;
    memcpy(&slen1, ((uint8_t *)param + next), sizeof(int));
    next += sizeof(int);
    lpc_string *str1 = lpc_make_string("", slen1);
    memcpy(str1->string, ((uint8_t *)param + next), slen1);
    next += slen1;

    int slen2;
    memcpy(&slen2, ((uint8_t *)param + next), sizeof(int));
    next += sizeof(int);
    lpc_string *str2 = lpc_make_string("", slen2);
    memcpy(str2->string, ((uint8_t *)param + next), slen2);
    next += slen2;

    int slen3;
    memcpy(&slen3, ((uint8_t *)param + next), sizeof(int));
    lpc_string *str3 = lpc_make_string("", slen3);
    if (str3->slen < (str1->slen + str2->slen))
    {
        errno = ENOMEM;
        str3->slen = -1;
        memcpy(((uint8_t *)param + next), &str3->slen, sizeof(int));
        return -1;
    }
    strcat(str3->string, str1->string);
    strcat(str3->string, str2->string);
    next += sizeof(int);
    memcpy(((uint8_t *)param + next), str3->string, str3->slen);
    return 0;
}

int test_debordement(void *param)
{
    printf("je suis test_debordement \n");
    return 0;
}

int test_errno(void *param)
{
    return 0;
}