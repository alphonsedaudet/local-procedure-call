

/*
 * client_function_test.c
 * Quelques fonctions client pour tester le fonctionnement de LPC
 */

#ifndef LPC_TEST_FUNCTION_H_
#define LPC_TEST_FUNCTION_H_

/**
 * permute deux nombres entiers
*/
int permute_int(void *param);

/**
 * permute deux nombres déciamaux
*/

int permute_double(void *param);

/**
 * Concaténation de deux chaines de caractères
*/

int concat_string(void *param);

/**
 * test le débordement de la taille d'une de caractères lpc_string
*/
int test_debordement(void *param);

/**
 * retourne −1 et met une valeur dans errno
*/
int test_errno(void *param);

#endif /* LPC_TEST_FUNCTION_H_ */