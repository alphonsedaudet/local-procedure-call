/*
 * lpc_server.c
 *
 *  Created on: Mar 29, 2021
 *      Authors: daudet , bah
 */

#ifdef __linux__
#define _XOPEN_SOURCE 500
#endif

#include "../utils.h"
#include "../lpc_type.h"
#include "lpc_server.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#define NB_FUNCTION 5

/**
 * inclure les fichiers de fonction client
 * */
#include "function/client_function_test.h"

/**
 * tableau des fonctions client 
 * */
static lpc_function functions[NB_FUNCTION] = {
    {"permute_int", &permute_int},
    {"permute_double", &permute_double},
    {"concat_string", &concat_string},
    {"test_debordement", &test_debordement},
    {"test_errno", &test_errno}};

static int init_process_synch_memory(synch_memory *memo, const char *memo_name)
{
    int code = initialiser_mutex(&memo->mutex);
    if (code > 0)
    {
        thread_error(__FILE__, __LINE__, code, "initialiser_mutex");
        return code;
    }
    code = initialiser_cond(&memo->client_cond);
    if (code != 0)
    {
        thread_error(__FILE__, __LINE__, code, "init_rcond");
        return code;
    }

    code = initialiser_cond(&memo->server_cond);
    if (code != 0)
    {
        thread_error(__FILE__, __LINE__, code, "init_wcond");
        return code;
    }
    memo->request = false;
    strcpy(memo->momory_name, memo_name);
    return 0;
}

static int init_process_com_memory(void *mem, const char *memo_name)
{
    memory *data = mem;
    int code = initialiser_mutex(&data->mutex);
    if (code > 0)
    {
        thread_error(__FILE__, __LINE__, code, "initialiser_mutex");
        return code;
    }
    code = initialiser_cond(&data->client_cond);
    if (code != 0)
    {
        thread_error(__FILE__, __LINE__, code, "init_rcond");
        return code;
    }
    code = initialiser_cond(&data->server_cond);
    if (code != 0)
    {
        thread_error(__FILE__, __LINE__, code, "init_wcond");
        return code;
    }
    data->turn = CLIENT;
    strcpy(data->memory_name, memo_name);
    return 0;
}

void *lpc_create(const char *nom, size_t capacite)
{
    if (sysconf(_SC_THREAD_PROCESS_SHARED) < 0)
    {
        fprintf(stderr, "sorry, process shared mutexes not supported\n");
        return NULL;
    }

    char *shmo_name = prefix_slash(nom);
    shm_unlink(shmo_name);

    int fd;
    if ((fd = shm_open(shmo_name, O_CREAT | O_RDWR, S_IWUSR | S_IRUSR)) < 0)
    {
        PANIC("shm_open");
        return NULL;
    }

#ifdef __linux__
    if (ftruncate(fd, capacite) < 0)
    {
        PANIC("ftruncate");
        return NULL;
    }
#endif

#ifdef __APPLE__
    struct stat st;
    if (fstat(fd, &st) < 0)
    {
        PANIC("fstat");
        return NULL;
    }
    if (st.st_size == 0)
    {
        if (ftruncate(fd, memory_size) < 0)
        {
            PANIC("ftruncate");
            return NULL;
        }
    }
    else if (st.st_size != memory_size)
    {
        fprintf(stderr, "l'objet memoire %s existe mais la taille incrrecte. Suppression. \n", argv[1]);
        shm_unlink(shmo_name);
        fprintf(stderr, "Relancer le processus %s.\n", argv[0]);
        execv(argv[0], argv);
        PANIC("exec");
    }
#endif

    free(shmo_name);

    memory *mem = mmap(NULL, capacite, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if ((void *)mem == MAP_FAILED)
    {
        PANIC("mmap");
        return NULL;
    }

    return mem;
}

void *lpc_get_function(const char *nom)
{
    for (size_t i = 0; i < NB_FUNCTION; i++)
        if (strcmp(functions[i].fun_name, nom) == 0)
            return &functions[i];
    return NULL;
}

void parallel_child_processing(char *synch_memo_name, pid_t client_pid)
{
    /**
     * taitement du processus fils du server
     * */

    char com_memo_name[NAMELEN];

    if (sprintf(com_memo_name, "%s_%d", synch_memo_name, client_pid) < 0)
        perror("sprintf");

    /*creation et projecte la mémoire de communication */
    memory *com_memory = lpc_create(com_memo_name, sizeof(memory));
    init_process_com_memory(com_memory, com_memo_name);

    /**
     * débloque le processus client qui attend une projection mémoire par le fils du serveur
     * */

    char syn_file[NAMELEN] = "/dev/shm/synch_file";
    strcat(syn_file, com_memo_name);
    open(syn_file, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

    /* demander le verous sur la mutex */
    int code = pthread_mutex_lock(&com_memory->mutex);
    if (code != 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    while (!(com_memory->turn == SERVER))
    {
        /*le serveur attens que le client remplit la mémoire partagé */
        code = pthread_cond_wait(&com_memory->server_cond, &com_memory->mutex);
        if (code > 0)
            thread_error(__FILE__, __LINE__, code, "pthread_cond_wait");
    }
    /**
     * le client a remplit la mémoire et a envoyé  un signal au serveur et s'est mis en attente
     * le serveur recoit le signal, se reveille et exécute la function du client
    */

    lpc_function *my_function = (lpc_function *)lpc_get_function(com_memory->fun_name);

    if (my_function == NULL)
        com_memory->response = -1;
    else if ((com_memory->response = my_function->fun(com_memory->data)) == -1)
        com_memory->errno_code = errno;

    com_memory->turn = CLIENT;

    code = pthread_mutex_unlock(&com_memory->mutex);
    if (code != 0)
        thread_error(__FILE__, __LINE__, code, "mutex_unlock");

    /* le serveur notifie le client*/

    code = pthread_cond_signal(&com_memory->client_cond);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "cond_signal");

    code = pthread_mutex_lock(&com_memory->mutex);
    if (code != 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    /** le serveur se met en attente 
     *  attend que le client termine de copier les resultats et envoie son siganal
     */

    while (!(com_memory->turn == SERVER))
    {
        code = pthread_cond_wait(&com_memory->server_cond, &com_memory->mutex);
        if (code > 0)
            thread_error(__FILE__, __LINE__, code, "pthread_cond_wait");
    }

    code = pthread_mutex_unlock(&com_memory->mutex);
    if (code != 0)
        thread_error(__FILE__, __LINE__, code, "mutex_unlock");

    /** 
     * le serveur a recut le signal du client
     * le serveru supprime la mémoire partagé et ensuite 
     * */

    shm_unlink(com_memo_name);
    unlink(syn_file);
    printf("le fils %d termine \n", getpid());
    exit(EXIT_SUCCESS);
}

void accept(synch_memory *synch_memo)
{
    int code;
    while (!synch_memo->request)
    {
        /** le serveur attent jusqu'a recevoir un sigal indiquant une requette disponible
         *  l'or de l'attente, le serveur libère le verous sur la mutex , se met en attente sur la condition server_cond 
         * */
        code = pthread_cond_wait(&synch_memo->server_cond, &synch_memo->mutex);
        if (code > 0)
            thread_error(__FILE__, __LINE__, code, "pthread_cond_wait");
    }

    pid_t client_pid = synch_memo->client_pid;

    printf("le serveur fait accept au client %d \n", client_pid);

    char synch_memo_name[50];

    strcpy(synch_memo_name, synch_memo->momory_name);

    pid_t pid;
    if ((pid = fork()) < 0)
    {
        PANIC("fork");
        exit(EXIT_FAILURE);
    }
    else if (pid == 0)
    {
        if ((pid = fork()) < 0)
        {
            PANIC("fork");
            exit(EXIT_FAILURE);
        }
        else if (pid == 0)
            parallel_child_processing(synch_memo_name, client_pid);
        exit(EXIT_SUCCESS);
    }
    pid_t dead_process;
    while ((dead_process = wait(NULL)) > 0)
        ;
}

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "donnez un nom au serveur \n");
        exit(EXIT_FAILURE);
    }
    synch_memory *synch_memo = lpc_create(argv[1], sizeof(synch_memory));
    init_process_synch_memory(synch_memo, argv[1]);

    printf("Le serveur peut déja recevoir les clients \n");

    while (true)
    {
        /* demander le verous sur la mutex */
        int code = pthread_mutex_lock(&synch_memo->mutex);
        if (code != 0)
            thread_error(__FILE__, __LINE__, code, "mutex_lock");

        /**
          *  - le serveur à obtenu le verrous sur la variable mutex de la synch_memory
          *  - le serveur fait accept et attent jusqu'à ce que un client envoi une requette via la mémoire
          *  - accept confie la requette au fils du serveur et retourne
          */

        printf("pret a recevoir une requete \n");

        accept(synch_memo);

        /*le serveur libère le verrous sur la mémoire synch_memory et notifi les clients en attentes*/

        synch_memo->request = false;

        code = pthread_mutex_unlock(&synch_memo->mutex);
        if (code != 0)
            thread_error(__FILE__, __LINE__, code, "mutex_unlock");

        code = pthread_cond_signal(&synch_memo->client_cond);
        if (code > 0)
            thread_error(__FILE__, __LINE__, code, "cond_signal");
    }

    return 0;
}
