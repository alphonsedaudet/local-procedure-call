
#include "lpc_client.h"
#include "../lpc_type.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "entrez le nom du serveur plc en marche \n");
        exit(EXIT_FAILURE);
    }

    int taille = strlen(argv[1]);
    char shmo_name[taille];

    strcpy(shmo_name, argv[1]);

    void *mem = lpc_open(shmo_name, sizeof(synch_memory));

    int response;

    /**
     *  test 1 :  test avec le type  int
     * permute_int(void*) fait la permuation de a et b
     * */
    int a = 15, b = 5;

    response = lpc_call(mem, "permute_int", INT, &a, INT, &b, NOP);

    if (response == 0 && a == 5 && b == 15)
        printf("Test for INT param PASS ! \n");
    else
        printf("Test for INT param FAIL ! \n");

    /**
     *  test 2 :  test avec le type  double
     * permute_int(void*) fait la permuation de a et b
     * */

    double c = 20, d = 5;

    response = lpc_call(mem, "permute_double", DOUBLE, &c, DOUBLE, &d, NOP);

    if (response == 0 && c == 5 && d == 20)
        printf("Test for DOUBLE param PASS ! \n");
    else
        printf("Test for DOUBLE param FAIL! \n");

    /**
     * test 3 : Test avec le type string
     * permute_int(void*) fait la permuation de string1 et string2
     * */

    lpc_string *str1 = lpc_make_string("universite de ", 50);
    lpc_string *str2 = lpc_make_string("paris", 50);
    lpc_string *result1 = lpc_make_string("", 100);

    response = lpc_call(mem, "concat_string", STRING, str1, STRING, str2, STRING, result1, NOP);

    if (response == 0 && strcmp(result1->string, "universite de paris") == 0)
        printf("Test for STRING param PASS! \n");
    else
        printf("Test for STRING value FAIL! \n");

    /**
     * test 4 : Test de débordement et errno pour le type string
     * taille insuffisante
     * */

    lpc_string *result2 = lpc_make_string("", 10);
    response = lpc_call(mem, "concat_string", STRING, str1, STRING, str2, STRING, result2, NOP);

    if (response == -1 && result2->slen == -1 && errno == ENOMEM)
        printf("Test for OVERFLOW STRING value PASS ! \n");
    else
        printf("Test for OVERFLOW STRING value FAIL ! \n");

    lpc_close(mem);
    free(str1);
    free(str2);
    free(result1);
    free(result2);

    /**
     * test 5 : Test du serveur distribue avec multi clients en parallèle
     * */

    pid_t pid;
    pid_t dead_process;
    int nb_client = 2;

    for (int i = 0; i < nb_client; i++)
    {
        if ((pid = fork()) < 0)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        else if (pid == 0)
        {
            void *mem_child = lpc_open(shmo_name, sizeof(synch_memory));
            int pid = getpid();
            char mypid[10];
            sprintf(mypid, "%d", pid);
            lpc_string *chaine1 = lpc_make_string("je suis le fils avec le pid = ", 50);
            lpc_string *chaine2 = lpc_make_string(mypid, 10);
            lpc_string *chaine_final = lpc_make_string("", 60);
            response = lpc_call(mem_child, "concat_string", STRING, chaine1, STRING, chaine2, STRING, chaine_final, NOP);
            char valid_resul[chaine_final->slen];
            strncpy(valid_resul, chaine1->string, strlen(chaine1->string));
            strcat(valid_resul, chaine2->string);
            if (response == 0 && strcmp(chaine_final->string, valid_resul) == 0)
                printf("Client %d Test for STRING param PASS ! \n", pid);
            else
                printf("Client %d Test for STRING param FAIL ! \n", pid);

            lpc_close(mem_child);
            free(chaine1);
            free(chaine2);
            free(chaine_final);
            exit(EXIT_SUCCESS);
        }
    }

    int burried_child_count = 0;
    while ((dead_process = wait(NULL)) > 0)
    {
        printf("le client  %d: termine !!\n", dead_process);
        burried_child_count++;
    }
    printf("Nombre de client terminé : %d\n", burried_child_count);

    return 0;
}
