#include "lpc_client.h"
#include "../utils.h"
#include "../lpc_type.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <stddef.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>

void *lpc_open(const char *name, size_t capacite)
{
    char *mem_name = prefix_slash(name);
    int fd;
    fd = shm_open(mem_name, O_RDWR, S_IWUSR | S_IRUSR);
    if (fd == -1)
        return NULL;
    void *mem = NULL;
    mem = mmap(NULL, capacite, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem == MAP_FAILED)
        return NULL;
    return mem;
}

static void connect(void *mem, char *memory_name)
{
    synch_memory *synch_memo = (synch_memory *)mem;

    int code;
    code = pthread_mutex_lock(&synch_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    synch_memo->client_pid = getpid();
    synch_memo->request = true;

    code = pthread_cond_signal(&synch_memo->server_cond);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    code = pthread_cond_wait(&synch_memo->client_cond, &synch_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "pthread_cond_wait");

    char com_memo_name[100];
    if (sprintf(com_memo_name, "%s_%d", synch_memo->momory_name, getpid()) < 0)
        perror("sprintf");
    strcpy(memory_name, com_memo_name);

    code = pthread_mutex_unlock(&synch_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_unlock");
}

static void wait_ready(char *file)
{
    char syn_file[256] = "/dev/shm/synch_file";
    strcat(syn_file, file);
    int fd;
    while ((fd = open(syn_file, O_WRONLY)) == -1 && errno == ENOENT)
        ;
    close(fd);
}

int lpc_call(void *mem, const char *fun_name, ...)
{

    int response;
    char memory_name[NAMELEN];
    connect(mem, memory_name);
    wait_ready(memory_name);

    memory *com_memo = lpc_open(memory_name, sizeof(memory));
    int code = pthread_mutex_lock(&com_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    va_list liste_parametres;
    va_start(liste_parametres, fun_name);
    lpc_type type_arg;
    size_t next = 0;
    int compteur = 0;
    lpc_param params[MAXPARAM];

    do
    {
        type_arg = va_arg(liste_parametres, lpc_type);

        switch (type_arg)
        {
        case STRING:
        {
            lpc_string *msg = va_arg(liste_parametres, lpc_string *);
            memcpy((com_memo->data + next), &msg->slen, sizeof(int));
            next += sizeof(int);
            memcpy((com_memo->data + next), msg->string, msg->slen);
            next += msg->slen;
            lpc_param p = {STRING, msg};
            params[compteur++] = p;
            break;
        }
        case DOUBLE:
        {
            double *msg = va_arg(liste_parametres, double *);
            memcpy((com_memo->data + next), msg, sizeof(double));
            next += sizeof(double);
            lpc_param p = {DOUBLE, msg};
            params[compteur++] = p;
            break;
        }
        case INT:
        {
            int *msg = va_arg(liste_parametres, int *);
            memcpy((com_memo->data + next), msg, sizeof(int));
            next += sizeof(int);
            lpc_param p = {INT, msg};
            params[compteur++] = p;
            break;
        }
        default:
            break;
        }

    } while (type_arg != NOP);

    strcpy(com_memo->fun_name, fun_name);
    com_memo->turn = SERVER;

    code = pthread_mutex_unlock(&com_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_unlock");

    /* signaler le serveur*/
    code = pthread_cond_signal(&com_memo->server_cond);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    code = pthread_mutex_lock(&com_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    while (!(com_memo->turn == CLIENT))
    {
        code = pthread_cond_wait(&com_memo->client_cond, &com_memo->mutex);
        if (code > 0)
            thread_error(__FILE__, __LINE__, code, "pthread_cond_wait");
    }

    /* copie le résultat */

    response = com_memo->response;
    next = 0;
    if (response == -1)
        errno = com_memo->errno_code;

    for (int i = 0; i < compteur; i++)
    {
        switch (params[i].type)
        {
        case STRING:
        {

            lpc_string *ptr = params[i].param;
            int slen;
            int tmplen = ptr->slen;
            memcpy(&slen, (com_memo->data + next), sizeof(int));
            memcpy(&ptr->slen, &slen, sizeof(int));
            next += sizeof(int);
            if (response == 0)
            {
                memcpy(ptr->string, (com_memo->data + next), slen);
                next += slen;
            }
            else
                next += tmplen;
            break;
        }
        case DOUBLE:
        {
            if (response == 0)
                memcpy(params[i].param, (com_memo->data + next), sizeof(double));
            next += sizeof(double);
            break;
        }
        case INT:
        {
            if (response == 0)
                memcpy(params[i].param, (com_memo->data + next), sizeof(int));
            next += sizeof(int);
            break;
        }
        default:
            break;
        }
    }

    com_memo->turn = SERVER;

    /*  mutex_unlock */
    code = pthread_mutex_unlock(&com_memo->mutex);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_unlock");

    /* signaler le serveur*/
    code = pthread_cond_signal(&com_memo->server_cond);
    if (code > 0)
        thread_error(__FILE__, __LINE__, code, "mutex_lock");

    return response;
}

int lpc_close(void *mem)
{
    int code;
    if ((code = munmap(mem, sizeof(memory))) != 0)
        perror("munmap");
    return code;
}
