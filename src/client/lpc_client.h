/*
 * lpc_client.h
 *
 *  Created on: Mar 29, 2021
 *      Author: 
 */

#ifndef LPC_CLIENT_H_
#define LPC_CLIENT_H_

#include <stddef.h>
/**
 * La fonction ouvre le shared memory object dont le nom est name, fait sa projection en
 * mémoire et retourne l’adresse de cette mémoire. Si le shared mémory object n’existe pas,
 * alors lpc_open échoue et retourne NULL.
 */
void *lpc_open(const char *name, size_t capacite);

/**
 * exécuter par un client lorsqu’il ne veut plus faire d’appels aux fonctions du serveur
 * suppreme les structures de données que le client aurait pu allouer localement
 */
int lpc_close(void *mem);

/**
 * Un processus client utilisera lpc_call pour faire appel à une fonction du serveur.
 */
int lpc_call(void *memory, const char *fun_name, ...);

#endif /* LPC_CLIENT_H_ */
